<html>
    <head><title>Branches - Partiturgenerator</title></head>
    <body bgcolor="#ffffff">
        <?php

            // Maximum length (in minutes)
            $maxmin = 720;
            
            function atoi($string) {
                $num = 0;
                for ( $i = 0 ; $i < strlen($string) ; $i++ ) {
                    $a = ord($string[$i]) - 0x30;
                    if ( $a >= 0 && $a <= 9 )
                        $num = $num * 10 + $a;
                }
                return $num;
            }

            function printmin($min) {
                if ($min > 59)
                    printf ("&nbsp;%u:%02u", floor($min / 60), $min % 60);
                else
                    printf ($min);
            }

            function pause($ttime, $itime) {
                $ptime = rand(1, 8);

                if ( $itime + $ptime > $ttime )
                    $ptime = $ttime - $itime;

                print("<tr><td align=\"right\"><tt><font size=\"3\">");
                printmin($itime);
                print("&nbsp;</font></tt></td><td align=\"middle\"><tt><font size=\"3\">$ptime</font></tt></td><td><tt><font size=\"3\">&nbsp;Pause&nbsp;</font></tt></td></tr>\n");

                return $ptime;
            }

            // Retrieve Boundaries
            $mins = atoi($_GET['min']);
            $count = atoi($_GET['num']);
            $scot = ( $_GET['cot'] == "1" );
            $head = ( $_GET['head'] == "0" );

            // Make sure we keep it halfway sane
            if ( $mins < 1 ) $mins = 1;
            if ( $mins > $maxmin ) $mins = $maxmin;

            if ( $count < 1 ) $count = 1;
            if ( $count > 50 ) $count = 50;
    
            // Header
            if ( ! $head ) {
                print "<h2>Branches - Partituren v1.3</h2>";
                if ( $count > 1 )
                    print "<p>$count Durchg&auml;nge &agrave; $mins Minuten</p>\n";
                else
                    print "<p>1 Durchgang &agrave; $mins Minuten</p>\n";
            }

            // Calculate Child of Trees
            for ( $durchgang = 0 ; $durchgang < $count ; $durchgang++ ) {
                print "<p><table border=\"1\">\n<tr><td><tt>&nbsp;Zeit&nbsp;</tt></td><td align=\"middle\"><tt>&nbsp;Min&nbsp;</tt><td><tt>&nbsp;Instrumente&nbsp;</tt></td></tr>\n";

                $time = 0;

                if ( ($scot == 0 || $durchgang > 0) && rand(0,1) ) {
                    $time += pause($mins, $time);
                }
                while ( $time < $mins ) {
                    // Actual Child of Tree
                    $slices = 0; $total = 0;

                    // Calculate number of slices and their individual time
                    while ( $total < 8 ) {
                        $nlen = rand(1,4);
                        if ( $total + $nlen > 8 )
                            $nlen = 8 - $total;
                        $slens[$slices++] = $nlen;
                        $total += $nlen;
                    }

                    // Make all instruments available in random order
                    for ( $i = 0 ; $i < 9 ; $i++ )
                        $inst[$i] = 0;
                    for ( $i = 1 ; $i <= 9 ; $i++ ) {
                        $j = rand(0, 8);
                        while ($inst[$j] > 0) if (++$j > 8) $j = 0;
                        $inst[$j] = $i;
                    }

                    // We need one actual Child of Tree (apart from the derivations), so
                    // make sure the first one is one
                    $nins = ( ( $scot && $durchgang == 0 && $time == 0 ) ? 9 : rand($slices, 9) );
                    $cot = ( $nins == 9 && $time == 0 );

                    $cultime = $time;
                    for ( $slice = 0 ; $slice < $slices ; $slice++ ) {
                        $cultime += $slens[$slice];
                        // How many instruments in this slice (making sure there will be enough left)
                        $slins = rand(0, $nins - $slices + $slice + 1) + 1;
    
                        // If this is the second to last slice (before the Rassel), use all remaining
                        // instruments
                        if ( $slice == $slices - 2 )
                            $slins = $nins;
    
                        // Make sure there's at least one instrument - this should never happen
                        if ( $slins < 1 )
                            $slins = 1;

                        // Determine instruments to use
                        if ( $slice == $slices - 1 ) {
                            $instrs[$slice] = "Rassel";
                        } else {
                            // New array to remember pickings
                            for ( $i = 0 ; $i < 9 ; $i++ )
                                $myins[$i] = 0;
                            for ( $actins = 0; $actins < $slins ; $actins++ ) {
                                $ins = rand(0, $nins - 1);
                                for ( $i = 0 ; $i < 9 ; $i++ ) {
                                    // Is this instrument available?
                                    if ( $inst[$i] > 0 ) {
                                        if ( $ins == 0 ) {
                                            // We'll take it!
                                            $myins[$i] = $inst[$i];
                                            // So it's not available anymore
                                            $inst[$i] = 0;
                                            $nins--;
                                            // found it, no need to look further
                                            $i = 9;
                                        } else
                                            $ins--;
                                    }
                                }
                            }
                            $instrs[$slice] = "";
                            // Add them in sorted order
                            $ins = 0;
                            for ( $i = 1 ; $i <= 9 ; $i++ ) {
                                for ( $j = 0 ; $j < 9 ; $j++ ) {
                                    if ( $myins[$j] == $i ) {
                                        if ( $ins++ > 0 )
                                            $instrs[$slice] = "$instrs[$slice],&nbsp;";
                                        $instrs[$slice] = "$instrs[$slice]$i";
                                    }
                                }
                            }
                        }
                        // Do we have enough time left? If not, cut it short
                        if ( $cultime > $mins ) {
                            $slens[$slice] -= ( $cultime - $mins );
                            $slices = $slice + ( $slens[$slice] > 0 ? 1 : 0 );
                        }
                    }

                    // Print the slices
                    print ("<tr><td align=\"right\"><tt><font size=\"3\">");
                    for ( $slice = 0; $slice < $slices ; $slice++ ) {
                        if ( $cot ) {
                            print ("*&nbsp;&nbsp;");
                            $cot = 0;
                        }
                        printmin($time);
                        print ("&nbsp;<br>");
                        $time += $slens[$slice];
                    }
                    print("</font></tt></td><td align=\"middle\"><tt><font size=\"3\">");
                    for ( $slice = 0; $slice < $slices ; $slice++ )
                        print("$slens[$slice]<br>");
                    print("</font></tt></td><td><tt><font size=\"3\">");
                    for ( $slice = 0; $slice < $slices ; $slice++ )
                        print("&nbsp;$instrs[$slice]&nbsp;<br>");
                    print ("</font></tt></td></tr>");

                    if ( $time < $mins )
                        $time += pause($mins, $time);
                }

                print "</table></p>\n\n";
            }
        ?>
    </body>
</html>
