from pippi import dsp 
from pippi.oscs import Osc 
import os
from random import randint
from random import uniform
from math import sqrt
import argparse

def printseconds(s):
    return "[%02li:%02li:%02li] " % ((s / 3600), (s % 3600) / 60, s % 60)

def printtype(typ):
    return {
        0: "Mixed, lengthy, up to 72s",
        1: "Mixed, up to 72s",
        2: "Mixed, lengthy, up to 120s",
        3: "Mixed, up to 120s",
        4: "Long, 5-120s",
        5: "Long, 5-72s",
        6: "Short, lengthy, up to 30s",
        7: "Short, up to 30s",
        8: "Short, lengthy, up to 10s",
        9: "Short, up to 10s"
    }.get(typ, "Randomly sway between types")

# Get a random frequency (within bounds)
def getrandfreq(mini, maxi):
    return uniform(mini, mini + maxi)

# Get one of the defined middles
def getfreq(f1, f2, variant):
    if variant == 0:
        print("Arithmetic - ", end='')
        return (f1 + f2) / 2
    elif variant == 1:
        print("Geometric  - ", end='')
        return sqrt( f1 * f2 )
    elif variant == 2:
        print("Harmonic   - ", end='')
        return ( 2 * f1 * f2  / ( f1 + f2 ) )
    else:
        print("No Middle  - ")
        return f1

# Initialisation
seconds = 0
singleamp = 0.4
harmonyamp = 0.25
omiddle = -1
freq = [ 0, 0, 0 ]
mfreq = [ 0, 0, 0 ]

# Read in options
parser = argparse.ArgumentParser(description='Calculate Dieter Jordi Music')
parser.add_argument('--seconds', '-s', type=int, default=600,
                   help='length of piece in seconds')
parser.add_argument('--minfreq', '-l', type=int, default=80,
                   help='low frequency boundary')
parser.add_argument('--maxfreq', '-m', type=int, default=1100,
                   help='high frequency boundary')
parser.add_argument('--usewav', '-w', type=bool, default=1,
                   help='whether to write wav-files (not mp3s)')
parser.add_argument('--chgtype', '-t', type=int, default=0,
                   help='type of changes to use')
parser.add_argument('--outfile', '-o', type=str, default='jordi.wav',
                   help='filename to write')
args = parser.parse_args()
outfile = args.outfile
maxseconds = args.seconds
chgtype = args.chgtype
minfreq = args.minfreq
maxfreq = args.maxfreq
usewav = args.usewav

# Check Frequencies
maxfreq = maxfreq - minfreq
if maxfreq < 10:
    print("Changing Frequencies due to nonsense parameters")
    maxfreq = 10

# Information
print("Encoding Time:        %s" % printseconds(maxseconds))
print("Type:                 %s" % printtype(chgtype))
#print("MP3-Bitrate:          %i" % 
#print("MP3-Compression:      %f" %
print("Frequency Range:      %i - %i Hz" % (minfreq, minfreq + maxfreq))
print()

out = dsp.buffer(length = maxseconds)
# Start actual work
while seconds < maxseconds:
    print(printseconds(seconds), end='')

    if chgtype >= 100:
        print("(t %i) ", chgtype - 100, end='')

    middle = randint(0, 3)
    # Slightly increase the chance of changing frequencies */
    if middle == 3:
        if omiddle >= 0:
            middle = omiddle
        else:
            middle = 0
    
    # Make sure we choose new frequencies the first time
    if omiddle < 0:
        omiddle = middle

    if middle == omiddle:
        # Choose new frequencies
        freq[0] = getrandfreq(minfreq, maxfreq)
        freq[1] = getrandfreq(minfreq, maxfreq)
    else:
        # Change one of them
        freq[randint(0,2)] = freq[2]

    omiddle = middle
    freq[2] = getfreq(freq[0], freq[1], middle)
    single = randint(0, 1)

    # decide new frequencies
    rotchairs = randint(0, 5)
    if rotchairs == 0:
        mfreq[0] = freq[0]
        mfreq[1] = freq[1]
        mfreq[2] = freq[2]
    elif rotchairs == 1:
        mfreq[0] = freq[0]
        mfreq[1] = freq[2]
        mfreq[2] = freq[1]
    elif rotchairs == 2:
        mfreq[0] = freq[1]
        mfreq[1] = freq[0]
        mfreq[2] = freq[2]
    elif rotchairs == 3:
        mfreq[0] = freq[1]
        mfreq[1] = freq[2]
        mfreq[2] = freq[0]
    elif rotchairs == 4:
        mfreq[0] = freq[2]
        mfreq[1] = freq[0]
        mfreq[2] = freq[1]
    elif rotchairs == 5:
        mfreq[0] = freq[2]
        mfreq[1] = freq[1]
        mfreq[2] = freq[0]

    print("Freq: %6.1f, %6.1f, %6.1f - " % (mfreq[0], mfreq[1], mfreq[2]), end='')
    which = 0

    # decide time:
    #      0(def):  Mixed, lengthy, up to 72s\n")
    #      1:       Mixed, up to 75s\n")
    #      2:       Mixed, lengthy, up to 120s\n")
    #      3:       Mixed, up to 120s\n")
    #      4:       Long, 5-120s\n")
    #      5:       Long, 5-75s\n")
    #      6:       Short, lengthy, up to 30s\n")
    #      7:       Short, up to 30s\n")
    #      8:       Short, lengthy, up to 12s\n")
    #      9:       Short, up to 10s\n")
    #      1 rep = 1/2 second
    if chgtype % 100 == 1:
        rep = (randint(0, 4) + 1 ) * (randint(0, 24) + randint(0, 24)) + 1
    elif chgtype % 100 == 2:
        rep = randint(0, 239) + 1
    elif chgtype % 100 == 3:
        rep = randint(0, 229) + 11
    elif chgtype % 100 == 4:
        rep = randint(0, 139) + 11
    elif chgtype % 100 == 5:
        rep = (randint(0, 4) + 1) * (randint(0, 6) + randint(0, 6)) + 1
    elif chgtype % 100 == 6:
        rep = randint(0, 29) + 1
    elif chgtype % 100 == 7:
        rep = (randint(0, 1) + 1) * (randint(0, 1) + 1) * randint(0, 3) + 1
    elif chgtype % 100 == 8:
        rep = randint(0, 9) + 1
    elif chgtype % 100 == 9:
        rep = (randint(0, 4) + 1 ) * (randint(0, 12) + randint(0, 11)) + 1
    else:
        rep = randint(0, 149) + 1

    # Sway from time to time (but not too often) */
    if chgtype >= 100 and randint(0, 25) == 0:
        chgtype = randint(0, 9) + 100

    # Make sure we won't get overdue
    if seconds + rep / 2 > maxseconds:
        rep = (maxseconds - seconds) * 2

    if single:
        acctype = 'single'
    else:
        acctype = 'accord'
    print("Len: %.1f (%s)" % (rep / 2, acctype))

    # Calculate actual sines
    osc1 = Osc(freq = mfreq[0], wavetable=dsp.SINE)
    osc2 = Osc(freq = mfreq[1], wavetable=dsp.SINE)
    osc3 = Osc(freq = mfreq[2], wavetable=dsp.SINE)

    # Fill a SoundBuffer with output from the osc
    if single:
        note = osc1.play(rep / 6) * singleamp + \
               osc2.play(rep / 6) * singleamp + \
               osc3.play(rep / 6) * singleamp
    else:
        # Mix it
        note = osc1.play(rep / 2) * harmonyamp & \
               osc2.play(rep / 2) * harmonyamp & \
               osc3.play(rep / 2) * harmonyamp

    out.dub(note, seconds)

    # Count up
    seconds += rep / 2

# Write the output buffer to the WAV file
out.write(outfile)

