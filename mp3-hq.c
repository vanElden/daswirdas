#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include <lame.h>

#define BUFRATE		1
//#define RATE		8192
#define RATE		44100
#define BITS		16		// MUST be 16 in the MP3-Version
#define BUFSIZE		(( RATE / 2 ) * ( BITS / 8 )) / BUFRATE
//#define AMPL16		10000
#define AMPL16		5000
#define AMPL8		30

#define ARIT	0
#define GEOM	1
#define HARM	2

unsigned int getrand(int max) {
	return (int) rand() % (max + 1);
}

double getrandfreq(int min, int max) {
	return (double) ((rand() % max) + min + (double) (((int) rand() % 100) / 100));
}

void mydebug(const char *format, va_list ap) {
	fprintf(stderr, format, ap);
}

double getfreq(double f1, double f2, int variant) {

	switch (variant) {
		case ARIT:
			fprintf(stderr, "Arithmetic - ");
			return (f1 + f2) / 2;
		case GEOM:
			fprintf(stderr, "Geometric  - ");
			/* return f1 + (f2 - f1) / 3; */
			return sqrt ( f1 * f2 );
		case HARM:
			fprintf(stderr, "Harmonic   - ");
			return ( 2 * f1 * f2 ) / ( f1 + f2 );
		default:
			fprintf(stderr, "No Middle  - ");
			return f1;
	}
}

int getsine(double f, unsigned long int s) {
	if ( BITS == 8 ) 
		return AMPL8 * sin(3.14159 * f * s / RATE);
	else
		return AMPL16 * sin(3.14159 * f * s / RATE);
}

/* Hmmm... This is quite superfluos; however, it's much clearer
   programming-style and will be janitorized by a good compiler
   anyway */
int getm(int w) {
	switch (w) {
		case 0: return ARIT;
		case 1: return GEOM;
		default: return HARM;
	}
}

lame_global_flags *initaudio() {

	int audio;
	lame_global_flags *gfp;


	gfp = lame_init();
	lame_set_in_samplerate(gfp,RATE);
	lame_set_num_channels(gfp,1);
	//lame_set_brate(gfp,8);
	lame_set_compression_ratio(gfp, 16);
	lame_set_bWriteVbrTag(gfp,0);
	lame_set_quality(gfp,2);
	lame_set_mode(gfp,3);

	lame_set_errorf(gfp, &mydebug);
	lame_set_debugf(gfp, &mydebug);
	lame_set_msgf(gfp, &mydebug);

	//lame_set_padding_type(gfp, 0);

	audio = lame_init_params(gfp);

	if ( audio < 0 ) {
		fprintf(stderr, "Could not init lame: %i\n", audio);
		exit(1);
	}

	return gfp;
}

unsigned char geth(int s) {
	//return (s / 256) % 256;
	return (s >> 8) & 0xff;
}

unsigned char getl(int s) {
	return s % 256;
}

void usage(char *pn) {
	fprintf(stderr, "Usage: %s [-s seconds] [-o filename] [-t type] [-l lowfreq] -h [highfreq] -w\n", pn);
	fprintf(stderr, "       seconds    Number of seconds to record (Default: 60)\n");
	fprintf(stderr, "       filename   Name of Output-MP3-File (Default: STDOUT)\n");
	fprintf(stderr, "       type       Type of Length of Notes:\n");
	fprintf(stderr, "                  0(def):  Mixed, lengthy, up to 72s\n");
	fprintf(stderr, "                  1:       Mixed, up to 72s\n");
	fprintf(stderr, "                  2:       Mixed, lengthy, up to 120s\n");
	fprintf(stderr, "                  3:       Mixed, up to 120s\n");
	fprintf(stderr, "                  4:       Long, 5-120s\n");
	fprintf(stderr, "                  5:       Long, 5-72s\n");
	fprintf(stderr, "                  6:       Short, lengthy, up to 30s\n");
	fprintf(stderr, "                  7:       Short, up to 30s\n");
	fprintf(stderr, "                  8:       Short, lengthy, up to 10s\n");
	fprintf(stderr, "                  9:       Short, up to 10s\n");
	fprintf(stderr, "                  10:      Randomly sway between the above\n");
	fprintf(stderr, "       lowfreq    Lowest Frequency in Hz (Default: 50)\n");
	fprintf(stderr, "       highfreq   Highest Frequency in Hz (Default: 1100)\n");
	fprintf(stderr, "       -w         Write .wav, not .mp3\n");
	exit(1);
}

void printseconds(long s) {
	fprintf(stderr, "[%02li:%02li:%02li] ", (s / 3600), (s % 3600) / 60, s % 60);
}	

int main(int ac, char *av[]) {

	lame_global_flags *audio;
	unsigned char buffer[BUFSIZE];
	unsigned char mpbuffer[BUFSIZE / 4];
	unsigned long int x;
	int p, r, rep;
	double mfreq[3], freq[3];
	int single, which, omiddle = -1, middle;
	signed int sine;
	int i, mplen;
	FILE *mpfile;
	long seconds, maxseconds;
	char opt;
	short int type;
	int minfreq, maxfreq;
	int usewav, showinfo = 0;

	/* Init */
	x = 0; seconds = 0;
	srand(time(NULL));
	audio = initaudio();

	/* Read Options */
	mpfile = stdout;
	maxseconds = 60;
	type = 0;
	minfreq = 50; maxfreq = 1100;
	usewav = 0;

	while ((opt = getopt(ac, av, "o:s:t:l:h:w")) >= 0)
		switch (opt) {
			case 'o':
				if ((mpfile = fopen(optarg, "w")) == NULL) {
					fprintf(stderr, "Error opening file '%s'\n", optarg);
					exit(1);
				}
				break;
			case 's':
				maxseconds = atoi(optarg);
				if ( maxseconds <= 0 ) {
					fprintf(stderr, "Invalid Time\n");
					exit(1);
				}
				break;
			case 't':
				type = atoi(optarg);
				if ( type <= 0 || type > 10 ) {
					fprintf(stderr, "Unknown Type\n");
					exit(1);
				}
				if ( type == 10 ) {
					/* Type 10 -> 100 + actual */
					type = getrand(9) + 100;
				}
				break;
			case 'l':
				minfreq = atoi(optarg);
				if ( minfreq < 1) {
					fprintf(stderr, "\nChanging Minimum Frequency to 50Hz\n");
					minfreq = 50;
				}
				break;
			case 'h':
				maxfreq = atoi(optarg);
				break;
			case 'w':
				usewav = 1;
				break;
			default:
				usage(av[0]);
		}

	/* Check Frequencies */
	maxfreq = maxfreq - minfreq;
	if ( maxfreq < 3 ) {
		fprintf(stderr, "\nChanging Frequencies due to nonsense parameters\n");
		maxfreq = 3;
	}

	/* Info */
	fprintf(stderr, "\nEncoding Time:    ");
	printseconds(maxseconds);
	fprintf(stderr, "\n");

	fprintf(stderr, "Type:             ");
	switch (type) {
		case 0:
			fprintf(stderr, "Mixed, lengthy, up to 72s\n"); break;
		case 1:
			fprintf(stderr, "Mixed, up to 72s\n"); break;
		case 2:
			fprintf(stderr, "Mixed, lengthy, up to 120s\n"); break;
		case 3:
			fprintf(stderr, "Mixed, up to 120s\n"); break;
		case 4:
			fprintf(stderr, "Long, 5-120s\n"); break;
		case 5:
			fprintf(stderr, "Long, 5-72s\n"); break;
		case 6:
			fprintf(stderr, "Short, lengthy, up to 30s\n"); break;
		case 7:
			fprintf(stderr, "Short, up to 30s\n"); break;
		case 8:
			fprintf(stderr, "Short, lengthy, up to 10s\n"); break;
		case 9:
			fprintf(stderr, "Short, up to 10s\n"); break;
		default:
			fprintf(stderr, "Randomly sway between types\n");
	}

	fprintf(stderr, "MP3-Bitrate:      %i\n", lame_get_brate(audio));
	fprintf(stderr, "MP3-Compression:  %f\n", lame_get_compression_ratio(audio));

	fprintf(stderr, "Frequency Range:  %i-%i Hz\n\n", minfreq, maxfreq + minfreq);

	/* Start actual work */
	while (seconds < maxseconds) {

		printseconds(seconds);
		if ( type >= 100 ) fprintf(stderr, "(t %i) ", type - 100);
		middle = getrand(3);
		/* Slightly increase the chance of changing frequencies */
		if ( middle == 3 ) {
			if ( omiddle >= 0 )
				middle = omiddle;
			else middle = 0;
		} 
		middle = getm(middle);

		if ( omiddle < 0 ) omiddle = middle;

		if ( middle == omiddle ) {
			freq[0] = getrandfreq(minfreq, maxfreq);
			freq[1] = getrandfreq(minfreq, maxfreq);
		} else 
			freq[getrand(2)] = freq[2];
		omiddle = middle;
		freq[2] = getfreq(freq[0], freq[1], middle);
		single = getrand(1);
		switch (getrand(5)) {
			case 0:
				mfreq[0] = freq[0];
				mfreq[1] = freq[1];
				mfreq[2] = freq[2];
				break;
			case 1:
				mfreq[0] = freq[0];
				mfreq[1] = freq[2];
				mfreq[2] = freq[1];
				break;
			case 2:
				mfreq[0] = freq[1];
				mfreq[1] = freq[0];
				mfreq[2] = freq[2];
				break;
			case 3:
				mfreq[0] = freq[1];
				mfreq[1] = freq[2];
				mfreq[2] = freq[0];
				break;
			case 4:
				mfreq[0] = freq[2];
				mfreq[1] = freq[0];
				mfreq[2] = freq[1];
				break;
			default:
				mfreq[0] = freq[2];
				mfreq[1] = freq[1];
				mfreq[2] = freq[0];
				break;
		}
		fprintf(stderr, "Freq: %6.1f, %6.1f, %6.1f - ", mfreq[0], mfreq[1], mfreq[2]);
		which = 0;

	    /* 0(def):  Mixed, lengthy, up to 72s\n");
	       1:       Mixed, up to 75s\n");
	       2:       Mixed, lengthy, up to 120s\n");
	       3:       Mixed, up to 120s\n");
	       4:       Long, 5-120s\n");
	       5:       Long, 5-75s\n");
	       6:       Short, lengthy, up to 30s\n");
	       7:       Short, up to 30s\n");
	       8:       Short, lengthy, up to 12s\n");
	       9:       Short, up to 10s\n");

		   1 rep = 1/2 second */
		switch (type) {
			case 1:
			case 101:
				rep = getrand(149) + 1;
				break;
			case 2:
			case 102:
				rep = (getrand(4) + 1 ) * (getrand(24) + getrand(24)) + 1;
				break;
			case 3:
			case 103:
				rep = getrand(239) + 1;
				break;
			case 4:
			case 104:
				rep = getrand(229) + 11;
				break;
			case 5:
			case 105:
				rep = getrand(139) + 11;
				break;
			case 6:
			case 106:
				rep = (getrand(4) + 1) * (getrand(6) + getrand(6)) + 1;
				break;
			case 7:
			case 107:
				rep = getrand(29) + 1;
				break;
			case 8:
			case 108:
				rep = (getrand(1) + 1) * (getrand(1) + 1) * getrand(3) + 1;
				break;
			case 9:
			case 109:
				rep = getrand(9) + 1;
				break;
			default:
				rep = (getrand(4) + 1 ) * (getrand(12) + getrand(11)) + 1; 
				break;
		}
		/* Sway from time to time (but not too often) */
		if ( type >= 100 && getrand(50) == 0 ) type = getrand(9) + 100;

		/* Make sure we won't get overdue */
		if ( seconds + rep / 2 > maxseconds ) rep = (maxseconds - seconds) * 2;
		fprintf(stderr, "Len: %.1f (%s)\n", (double) rep / 2, single ? "single" : "accord");

		/* Calculate actual sines */
		for ( r = 0 ; r < rep ; r++ ) {

			for ( p = 0; p < BUFSIZE ; p += (BITS / 8) * BUFRATE ) {

				/* Somewhat optimized for least actual hits
				   1 (0/0,1):  0 = -0.332, 1 = 0.332-0.666, 2 = 0.667-
				   2 (0/0,2):  0 = -0.666, 1 = 0.667-1.332, 2 = 1.333-
				   3 (1/2,0):  0 = -0.999, 1 = 1-1.999,     2 = 2-
				   4 (1/2,1):  0 = -1.332, 1 = 1.333-2.666, 2 = 2.667-
				   5 (1/2,2):  0 = -1.666, 1 = 1.667-3.332, 2 = 3.333-
				   6 (2/4,0):  0 = -1.999, 1 = 2-3.999,     2 = 4-
				   7 (2/4,1):  0 = -2.332, 1 = 2.333-4.666, 2 = 4.667-
				   8 (2/4,2):  0 = -2.666, 1 = 2.667-5.332, 2 = 5.333-
				   9 (3/6,3):  0 = -2.999, 1 = 3-5.999,     2 = 6- */
				if ( single ) {
					switch (which) {
						case 0:
								if (r == rep / 3 && ( rep % 3 == 0 || p >= ((rep % 3) * BUFSIZE) / 3))
										which = 1;
								break;
						case 1:
								/* beware the ()'s! (2*5)/3 != 2*(5/3) */
								if ( (r == 2 * (rep / 3)) && (rep % 3 == 0 || (rep % 3 == 1 && p >= 2 * BUFSIZE / 3)) )
										which = 2;
								else if ((r == 2 * (rep / 3) + 1) && (p >= BUFSIZE / 3))
									which = 2;
								break;
					}
				}
				/* TODO: Couldn't we get a smooth change from one sine
				 *       to the next by calculating a proper x? */

				if ( BITS == 8 ) {
                    x++;
					buffer[p] = 127;
					if ( single )
						buffer[p] += getsine(mfreq[which], x);
					else
						buffer[p] += getsine(mfreq[0], x)
							+ getsine(mfreq[1], x)
							+ getsine(mfreq[2], x);
				} else {
				    x += 2;
					buffer[p] = 0;
					buffer[p+1] = 0;

					if ( single ) {
						sine = getsine(mfreq[which], x);
						buffer[p]   += getl(sine);
						buffer[p+1] += geth(sine);
					} else {
						for ( i = 0 ; i < 3 ; i++ ) {
							sine = getsine(mfreq[i], x);
							buffer[p]   += getl(sine);
							buffer[p+1] += geth(sine);
						}
					}

/*                  if ( p + 11 > BUFSIZE )
						showinfo = 10;
					if ( showinfo-- > 0 ) {
						fprintf(stderr, "%i, %i (%6.1f), %i/%i:%i/%i\n", p, which, mfreq[which], x, sine, getl(sine), geth(sine));
					} */
				}
				
			}

			if ( usewav != 1 ) {
				mplen = lame_encode_buffer(audio, (short int *) buffer, NULL, BUFSIZE / (BITS / 8), mpbuffer, BUFSIZE / 4);
				if (mplen < 0) {
					fprintf(stderr, "Encoding failed: %i\n", mplen);
					exit(1);
				}
				write(fileno(mpfile), mpbuffer, mplen);
			} else {
				write(fileno(mpfile), buffer, BUFSIZE);
			}
		} /* for rep */
		seconds += rep / 2;
	} /* while seconds */

	if ( usewav != 1 ) {
		mplen = lame_encode_flush(audio, mpbuffer, BUFSIZE / 4);
		write(fileno(mpfile), mpbuffer, mplen);
		lame_mp3_tags_fid(audio, NULL);
		lame_close(audio);
	}

	exit(0);
}

// vim: ts=4
