from pippi import dsp, sampler, tune, rhythm
from pippi.oscs import Osc
import os
import random
import pyaudio

def play_d():
    cluster = dsp.buffer(channels = 1)
    chord = 'i^7'
    freqs = tune.chord(chord, octave=4, key='g')
    for freq in freqs:
        print("Frequency: %i" % freq)
        note = Osc(freq = freq, wavetable=dsp.SINE).play(1) * 0.2
        cluster.dub(note, 0)
    chord = 'ii^7'
    freqs = tune.chord(chord, octave=4, key='g')
    for freq in freqs:
        print("Frequency: %i" % freq)
        note = Osc(freq = freq, wavetable=dsp.SINE).play(1) * 0.2
        cluster.dub(note, 1)

    return cluster

def play_n2():
    samp = sampler.Sampler('violin2.wav', 'c5')
    cluster = dsp.buffer(channels = 1)
    key = 'f'
    chord = 'ii^7'
    freqs = tune.chord(chord, octave=3, key=key) \
            + tune.chord(chord, octave=4, key=key) \
            + tune.chord(chord, octave=5, key=key) \
            + tune.chord(chord, octave=6, key=key) \
            + [ tune.nts(key, 7) ]
            #+ tune.chord(chord, octave=7, key='g') \
    vol_curv = [ x / 100 for x in range(100, 0, -1) ]
    mid_num = int(len(freqs) / 2)

    i = 0
    for freq in sorted(freqs):
        keepalive = 3 * abs(i - mid_num)
        print("Frequency: %i, Keepalive for: %i" % ( freq, keepalive))
        #this_curv = [ 0.25, 0.5, 0.75 ] + [ 1.0 ] * (50 * keepalive + 1) + vol_curv
        this_curv = [ 1.0 ] * ((50 * keepalive) + 1) + vol_curv
        #note = Osc(freq = freq, wavetable=dsp.SINE).play(length = 5 + keepalive) * 0.02
        note = samp.play(freq) * 0.1
        #note = samp.play(freq).stretch(length = 1 + keepalive) * 0.1
        note = note.cut(length = 1 + keepalive) * this_curv
        #print("Note Length: %i" % note.dur)
        cluster.dub(note, 0)
        i += 1

    return cluster


def play_n():
    samp = sampler.Sampler('harpc2.wav', 'c2')
    cluster = dsp.buffer(channels = 1)
    chord = 'i^7'
    freqs = tune.chord(chord, octave=3, key='f') \
            + tune.chord(chord, octave=4, key='f') \
            + tune.chord(chord, octave=5, key='f') \
            + tune.chord(chord, octave=6, key='f') \
            + tune.chord(chord, octave=7, key='f') \
            + tune.chord(chord, octave=8, key='f')
    volumes = [ 2.3, 2.1, 1.9, 1.8, 1.7, 1.6, 1.5, 1.4, 1.3, 1.2, 1.1, 1.0,
                1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.1, 2.3 ]
    i = 0
    #print(sorted(freqs))
    while volumes[0] > 0:
        i = i + 1
        j = 0
        for freq in sorted(freqs):
            vol = volumes[j]
            if vol > 1:
                vol = 1
            if vol < 0:
                vol = 0
            #note = Osc(freq = freq, wavetable=dsp.SINE).play(0.2) * 0.005
            note = samp.play(freq) * vol * 0.1
            #print("Freq: %i, Vol: %f" % ( freq, vol ))
            cluster.dub(note, i * 0.2)
            volumes[j] -= .015
            j = j + 1

    return cluster

out = play_n2()
out.write('dsp_n.wav')



#PyAudio = pyaudio.PyAudio
#p = PyAudio()
#
#out = play_d()
#
#stream = p.open(format = pyaudio.paInt32, channels = out.channels,
#                rate = out.samplerate, output = True)
#
##outbuf = []
##for frame in out.frames:
#    #print("Frame: %s of type %s with shape %s" % ( frame, type(frame), frame.shape ))
#    #outbuf += frame[1]
#
##stream.write(outbuf)
#stream.write(out.frames)
